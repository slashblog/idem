import os

import pytest_idem.runner as runner
import yaml


def test_config(idem_cli):
    # Create a config template
    proc_template = idem_cli(
        "state",
        "--config-template",
        "--cache-dir=cache",
        check=True,
        encoding="ascii",
    )

    # Verify that output was created
    assert proc_template.stdout
    # Verify that the output is valid yaml
    assert yaml.safe_load(proc_template.stdout)

    with runner.named_tempfile(suffix=".cfg", mode="w+") as fh:
        # Write the config template to a config file
        fh.write_text(proc_template.stdout)

        # Run an idem process with the --config flag after the subcommand
        ret = idem_cli(
            "exec",
            "test.ping",
            "--output=json",
            f"--config={fh}",
            encoding="ascii",
            check=True,
        ).json
        assert ret == {
            "comment": [],
            "ref": "exec.test.ping",
            "result": True,
            "ret": True,
        }

        # Run an idem process with the --config flag before the subcommand
        ret = idem_cli(
            "exec",
            "test.ping",
            "--output=json",
            pre_subcommand_args=[f"--config={fh}"],
            encoding="ascii",
            check=True,
        ).json
        assert ret == {
            "comment": [],
            "ref": "exec.test.ping",
            "result": True,
            "ret": True,
        }


def test_idem_run_with_config_cwd(idem_cli, tests_dir):
    cwd = os.getcwd()
    test_dir = tests_dir / "sls" / "config"

    os.chdir(test_dir)
    try:
        ret = idem_cli(
            "state",
            f'--config={test_dir / "idem.cfg"}',
            f"--tree={test_dir}",
            "--esm-plugin=null",
            env={"ACCT_FILE": "", "ACCT_KEY": ""},
        )
        assert ret.result is True, ret.stderr or ret.stdout
        assert ret.json, ret.stderr or ret.stdout

    finally:
        os.chdir(cwd)


def test_idem_run_with_config_absolute(idem_cli, tests_dir):
    test_dir = (tests_dir / "sls" / "config").absolute()

    ret = idem_cli(
        "state",
        f'--config={test_dir / "idem.cfg"}',
        f"--tree={test_dir}",
        "--esm-plugin=null",
        env={"ACCT_FILE": "", "ACCT_KEY": ""},
    )
    assert ret.result is True, ret.stderr or ret.stdout
    assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_config_relative_dir(idem_cli, tests_dir):
    tree = tests_dir / "sls" / "config"
    config_file = (tree / "idem.cfg").relative_to(tests_dir.parent)

    ret = idem_cli(
        "state",
        f"--config={config_file}",
        f"--tree={tree}",
        "--esm-plugin=null",
        env={"ACCT_FILE": "", "ACCT_KEY": ""},
    )
    assert ret.result is True, ret.stderr or ret.stdout
    assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_config_no_tree(idem_cli, tests_dir):

    tree = tests_dir / "sls" / "config"
    config_file = (tree / "idem.cfg").relative_to(tests_dir.parent)

    ret = idem_cli(
        "state",
        f"--config={config_file}",
        "--esm-plugin=null",
        env={"ACCT_FILE": "", "ACCT_KEY": ""},
    )
    assert ret.result is True, ret.stderr or ret.stdout
    assert ret.json, ret.stderr or ret.stdout


def test_mutually_exclusive_progress_options(idem_cli):
    ret = idem_cli("state", "--progress", "--no-progress-bar")

    assert (
        "argument --no-progress-bar: not allowed with argument --progress" in ret.stderr
    )


def test_get_resource_only_with_resource_id(idem_cli):
    # config property is not set
    ret = idem_cli("state", "--config-template")
    assert ret.json["idem"]["get_resource_only_with_resource_id"] is False

    # config property is set
    ret = idem_cli("state", "--get-resource-only-with-resource-id", "--config-template")
    assert ret.json["idem"]["get_resource_only_with_resource_id"] is True


def test_default_outputter_null(idem_cli):
    # Idem's default outputter should be None, as it uses a different default for each subcommand
    ret = idem_cli("--config-template")
    data = yaml.safe_load(ret.stdout)
    assert data["rend"]["output"] is None
