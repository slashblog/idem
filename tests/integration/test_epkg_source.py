import os

from pytest_idem.runner import run_sls


def test_epkg_source(hub, tests_dir):
    ACCT_DATA = {
        "profiles": {
            "file": {
                "default": {"key1": "value1"},
                "test_profile": {
                    "sls_bundle": "I2rfkxXn36pUEW0CyBYmt7nQL9OStDzFz9TnygJpEEU="
                },
            }
        }
    }

    path = os.path.join(tests_dir, "sls", "epkg", "sls_bundle.epkg")
    ret = run_sls(
        ["entry"],
        sls_sources=[f"file://test_profile@{path}"],
        acct_data=ACCT_DATA,
    )

    assert len(ret) == 6, f"Expecting 6 states {ret}"
    for state, ret in ret.items():
        assert ret["result"] is True, state
