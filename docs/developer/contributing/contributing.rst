.. _CONTRIBUTING:

.. include:: ../../../CONTRIBUTING.rst

Contribution Guidelines
=======================

Before asking for a final review for a PR into an idem project, the following guidelines must be met:

Tests
-----

- Tests are written for changes
- Tests provide full coverage of the changed code

Documentation
-------------

- Docs are written for feature changes
- Functions have Typehinted parameters
- Code is sufficiently documented with comments
- Parameters are explained in detail in function docstrings
- rst-style examples of the function's usage are included in its docstring

Code Style
----------

- Code is readable and contains comments
- Code contains sufficient logging, including debug logging
- Errors are descriptive
- Follow POP best practices
  - Plugins are used instead of Classes wherever possible
  - POP code is accessed via the hub, not python import
  - Output with ``hub.log.debug()`` not ``print()``
  - The hub is not explicitly passed to functions
  - Code is organized in the filesystem in a meaningful way
  - There are no long files - code is separated into plugins with meaningful names (no massive ``utils.py`` file)
  - Plugins are organized in a way that will be easily merged with other projects
  - Subsystems have contracts to standardize plugin structure
  - ``ctx.test`` must be implemented in idem states
- Code is written in a re-usable way

Issues
------

When reporting a bug, the following criteria should be met:

- Bugs include complete steps to reproduce including

  - Bugs include a version report from ``pip freeze``
  - Bugs include the full cli command used to reach the error
  - Bugs include sanitized supporting sls/credential files
  - Bugs include output with ``--log-level=debug`` logging
  - Bugs include the full error output

Pull Requests
-------------

- All TODOs are resolved
- All comments by maintainers in code-review are marked "resolved" by maintainers
- All existing tests are passing in the PR pipeline
- The origin pipeline has all tests enabled
- The origin pipeline is visible to maintainers

Versioning
----------

- Backwards-incompatible changes get a major version bump
- New features get a minor version bump
- Bugfixes get a point version bump
