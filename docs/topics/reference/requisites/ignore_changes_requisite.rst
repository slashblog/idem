==============
Ignore Changes
==============

In SLS blocks, use ``ignore_changes`` to prevent parameters from being updated on brown-field resources.

If a parameter is specified under ``ignore_changes`` then this parameter will be overridden with None and present() function will ignore updating such None-value parameters.
Therefore only 'Optional' parameters can be included in ``ignore_changes``, required parameters listed under ``ignore_changes`` are ignored.

In the following example, State_A is a green-field resource. During the first Idem run, Idem will create State_A resource with
the tags value. However, since ``ignore_changes`` contains ``tags``, during the second Idem run to update the State_A resource, Idem
will not update tags even when the tags of the resource has been deviated away from the initial {tag-key: tag-value} value.

.. code-block:: sls

   State_A:
      cloud.instance.present:
        - name: my-resource
        - tags: {tag-key: tag-value}
        - ignore_changes:
          - tags

Nested data under a parameter can be specified with a syntax similar to what arg_binding use: ':' for traversing a dict structure and '[]' for traversing a list.
In addition, '[*]' can be used to traverse through all elements in a list.
For example: Given the tags input below, if we want to just ignore changing tag1, we can do tags[0]. Though, we need to be careful on ignoring list type data with indexing here, since the order of list elements is not guaranteed in Idem.

.. code-block:: sls

    tags:
      - Name: tag1
        Value: value1
      - Name: tag2
        Value: value2

If the parameter path under ``ignore_changes`` is invalid, Idem will output a warning message but it won't fail the resource management operation.

Note that the ``ignore_changes`` requisite only takes effect on a brown-field resource. That is, the enforced State_A exists in ESM cache or the resource_id has been supplied in the SLS file.
