import inspect

__contracts__ = ["resource"]

from dataclasses import make_dataclass, field
from typing import List

from dict_tools.typing import Computed

SKIP_ESM = True


TREQ = {
    "present": {
        "require": [
            "test.present",
        ],
    },
}


def __init__(hub):
    hub.states.test_resource.ACCT = ["test"]


def present(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    old_state=None,
    changes=None,
    new_state=None,
    result=True,
    force_save=None,
    skip_esm=True,
    recreate_resource_without_resource_id: str = False,
    computed_str: Computed[str] = None,
    test_ambiguous_computed: str = None,
    dataclass: List[
        make_dataclass(
            "DataclassComputedStr",
            [
                ("test", str, field(default=None)),
                ("test_ambiguous_computed", Computed[str], field(default=None)),
                ("dataclass_computed_str", Computed[str], field(default=None)),
            ],
        )
    ] = None,
    **kwargs,
):
    """
    Return the previous old_state, if it's not specified in sls, and the given new_state.
    Raise an error on fail
    """

    if resource_id and recreate_resource_without_resource_id:
        return {
            "result": False,
            "comment": "get failed because resource_id is set",
            "ret": "error",
            "name": name,
            "old_state": old_state,
            "new_state": None,
        }

    if old_state is None:
        old_state = ctx.get("old_state")
    ret = {
        "name": name,
        "old_state": old_state,
        "new_state": new_state,
        "changes": changes,
        "result": result,
        "comment": None,
    }

    if force_save is not None:
        ret["force_save"] = force_save

    if skip_esm is not None:
        ctx.skip_esm = skip_esm

    return ret


def absent(
    hub,
    ctx,
    name: str,
    old_state=None,
    changes=None,
    new_state=None,
    result=True,
    force_save=None,
    **kwargs,
):
    if old_state is None:
        old_state = ctx.get("old_state")
    ret = {
        "name": name,
        "old_state": old_state,
        "new_state": new_state,
        "changes": changes,
        "result": result,
        "comment": None,
    }
    if force_save is not None:
        ret["force_save"] = force_save
    return ret


async def describe(hub, ctx):
    """
    Get the functions
    """
    ret = {}
    for func in hub.states.test:
        name = func.__name__
        if name not in ("present", "absent"):
            continue
        ref = f"test.{name}"
        state_name = f"Description of {ref}"
        ret[state_name] = {ref: []}

        # Collect args
        for arg, p in func.signature.parameters.items():
            if arg in ("hub", "ctx", "kwargs"):
                continue
            elif arg == "name":
                ret[state_name][ref].append({arg: name})
            else:
                if p.default == inspect._empty:
                    ret[state_name][ref].append({arg: None})
                else:
                    ret[state_name][ref].append({arg: p.default})

    return ret
