from pytest_idem.runner import run_yaml_block

STATE = """
state:
  skip.present:
    - skip: {skip}
"""


def test_skip():
    """
    Verify that the state is skipped when a pre contract returns "False"
    """
    ret = run_yaml_block(STATE.format(skip=True))
    state_ret = next(iter(ret.values()))
    assert "Skipped" in state_ret["comment"]
    assert state_ret["result"] is None


def test_no_skip():
    """
    Verify that the state runs and succeeds when a pre contract returns "True"
    """
    ret = run_yaml_block(STATE.format(skip=False))
    state_ret = next(iter(ret.values()))
    assert "Skipped" not in state_ret["comment"]
    assert state_ret["result"] is True, state_ret["comment"]


def test_nested_count(hub):
    """
    Verify that nested output counts skipped states
    """
    ret = run_yaml_block(STATE.format(skip=True))
    output = hub.output.state.display(ret)
    assert "present: 1 skipped" in output
    assert "- Skipped" in output


EVENT_STATE = """
event:
  event.flush
"""


def test_skipped_event(hub):
    """
    Verify that a skipped state results in an event being fired
    """
    ret = run_yaml_block(STATE.format(skip=True) + EVENT_STATE, runtime="serial")
    event_ret = ret["event_|-event_|-event_|-flush"]
    assert event_ret["result"], event_ret["comment"]
    assert event_ret["new_state"]
    for event in event_ret["new_state"]:
        body = str(event.body)
        if '"comment": ["Skipped",' in body and '"result": null' in body:
            break
    else:
        assert False, "Skip event not found in "
