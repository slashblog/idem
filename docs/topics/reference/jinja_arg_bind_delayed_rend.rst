=====================
Delays Between States
=====================

Idem SLS files support a dependency delay between states, where a state isn't rendered until a preceding state has been rendered.

.. code-block:: sls

    State_A:
      test.nop
    #!require: State_A
    State_B:
      test.nop

You can use a delay to parse and process an argument binding reference in a Jinja template. A Jinja template argument binding reference follows this pattern:

.. code-block:: sls

    {% for key,value in hub.idem.arg_bind.resolve('${<cloud>:<state>}').items() %}
        {{ value }}
    {% endfor %}

In the following example, State_B includes a Jinja template argument binding reference that needs a value from State_A. The ``#!require:State_A`` delay forces the rendering of State_B to first wait for State_A to be rendered, which makes its ``Name`` tag value available to State_B.

.. code-block:: sls

    State_A:
      cloud.subnetwork.search:
        - tags: {Name: {% params.get("pvt_subnetwork_name") %}}

    #!require:State_A

    State_B:
      cloud.private_cloud_attachment.present:
        - name: "Private cloud B"
        - subnetwork_ids: {% subnetwork_ids=[] %} {% for k,v in  hub.idem.arg_bind.resolve('${cloud.subnetwork:State_A}').items() %} {{ subnetwork_ids.append(v["resource_id"]) }} {% endfor %}

Fetching Argument Binding Reference Values
==========================================

For a Jinja template containing an argument binding reference to be rendered, the argument binding must be passed to custom function ``hub.idem.arg_bind.resolve()`` as a string. The function parses the argument binding template and resolves the value.

The ``new_state`` of the prerequisite block executed states should be available in ``hub.idem.RUNS[name]["running"]`` using the argument binding references in Jinja that were resolved in the ``rend`` subsystem.

For List, data resolution happens as follows:

The target 'foo:bar:[0]' or 'foo:bar[0]' will return data['foo']['bar'][0] if data like {'foo':{'bar':['baz']}}

For Dict,  data resolution happens as follows:

The target 'foo:bar:0' will return data['foo']['bar'][0] if data like {'foo':{'bar':{'0':'baz'}}}
