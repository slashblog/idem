import pytest_idem.runner as runner

STATE = r"""
delay_02:
  time.sleep:
    - duration: 2

delayed_render_block:
  render.block:
    - sls: >
       {% raw %}

       new_state:
         exec.run:
           - path: test.ping

       {% endraw %}

    - require:
       - time: delay_02
"""


def test_example(idem_cli):
    """
    Test the example in the render.block state documentation
    """
    with runner.named_tempfile(delete=True, suffix=".sls") as tmp:
        with open(tmp, "wb+") as fh:
            fh.write(STATE.encode())
            fh.flush()

        ret = idem_cli("state", tmp)

    # Verify that the sleep was successful
    assert ret.json["time_|-delay_02_|-delay_02_|-sleep"]["result"] is True
    # Verify that the state rendered
    assert (
        ret.json["render_|-delayed_render_block_|-delayed_render_block_|-block"][
            "result"
        ]
        is True
    )
    # Verify that the new state was added and ran
    assert ret.json["exec_|-new_state_|-new_state_|-run"]["result"] is True


STATE1 = r"""
delayed_render_block:
  render.block:
    - sls: >
        !syntax error!
"""


def test_failed_render(idem_cli):
    """
    Test a delayed render block with a failed render
    """
    with runner.named_tempfile(delete=True, suffix=".sls") as tmp:
        with open(tmp, "wb+") as fh:
            fh.write(STATE1.encode())
            fh.flush()

        ret = idem_cli("state", tmp)

    # Verify that the state rendered
    assert (
        ret.json["render_|-delayed_render_block_|-delayed_render_block_|-block"][
            "result"
        ]
        is False
    )
    assert any(
        "RenderException" in c
        for c in ret.json[
            "render_|-delayed_render_block_|-delayed_render_block_|-block"
        ]["comment"]
    )


STATE2 = r"""
{% set name = "delay_02" %}
{{ name }}:
  time.sleep:
    - duration: 2

delayed_render_block:
  render.block:
    - sls: >
       {% raw %}

       {% set name = "new_state" %}

        # If "name" is "new_state", then it was properly rendered within the state

       {{ name }}:
         exec.run:
           - path: test.ping

       {% endraw %}

    - require:
       # If "name" is "delay_02", it wasn't overridden by "set name" inside the "raw" jinja block
       - time: {{ name }}
"""


def test_jinja(idem_cli):
    """
    Verify that jinja in the delayed render block is skipped in the first render, but not the second
    """
    with runner.named_tempfile(delete=True, suffix=".sls") as tmp:
        with open(tmp, "wb+") as fh:
            fh.write(STATE2.encode())
            fh.flush()

        ret = idem_cli("state", tmp)

    # Verify that the sleep was successful
    assert ret.json["time_|-delay_02_|-delay_02_|-sleep"]["result"] is True
    # Verify that the state rendered
    assert (
        ret.json["render_|-delayed_render_block_|-delayed_render_block_|-block"][
            "result"
        ]
        is True
    )
    # Verify that the new state was added and ran
    assert ret.json["exec_|-new_state_|-new_state_|-run"]["result"] is True
