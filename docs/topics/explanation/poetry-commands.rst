.. _poetry-commands:

========================
Poetry commands for Idem
========================

With Poetry, you can quickly install packages, update package versions, or synchronize package versions.

The packages you add to your Idem project and their version requirements are recorded to `pyproject.toml`, which allows you to see the packages you installed and the version requirements you have set for them. The `poetry.lock` file maintains the same versions of all packages that are installed.

Poetry provides several commands that you can use for package management.

Install packages
================

To install packages, you can use `poetry add`.

.. code-block:: bash

    poetry add 'idem-aws@latest'
    poetry add 'idem-aws@2.1.1'

These two lines save the same project version requirement. `latest` is saved as a version number in `pyproject.toml`.

Install alternative packages
============================

Poetry can install packages from private repositories or directly from a git repo. This is not ideal for production environments, but it is useful for development environments.

For example, the following line in `pyproject.toml` installs the latest commit from the master branch.

.. code-block:: bash

    idem-aws = { git = "git@gitlab.com:vmware/idem/idem-aws.git" }

The git commit used is logged to `poetry.lock`. For more information, see `Git dependencies <https://python-poetry.org/docs/dependency-specification/#git-dependencies>`_ in the Poetry documentation.

Synchronize package versions
============================

To ensure that your virtual environment uses the same packages that are saved to `poetry.lock`, you can run `poetry install --sync`.

`poetry.lock` is updated when you add, remove, or upgrade packages using Poetry.

Update package versions
=======================

If you need to update packages, you can use `poetry update` and `poetry add`.

For example, `poetry update idem-aws` updates `idem-aws` to the latest version matching the specifiers in `pyproject.toml`. The command also bumps any sub-dependencies that are required.

The command `poetry add idem-aws@latest` updates the version specifier in `pyproject.toml` to the newest version available in pypi and installs that package.

The command `poetry update` with no other parameters updates all non-specified packages to the newest versions compatible with your specified packages.

Automatically update packages when checking out versions
========================================================

`poetry.lock` lists the installed plugin versions. After you add `poetry.lock` to git, the file is tracked with your configuration.

After checking out code, you can run `poetry install --sync`. This command installs the exact package versions specified in `poetry.lock`.

The flag `--sync` ensures that packages that are not specified are removed. Packages are normally removed when you run `poetry remove package`, but this doesn't happen by default with `poetry install`.

You can add this script to .git/hook/post-checkout. Every time you switch branches, Poetry installs the plugins the code was developed with.

.. code-block:: bash

    #!/bin/bash
    ~/.local/bin/poetry install --sync
