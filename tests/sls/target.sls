the_target:
  test.succeed_without_changes:
    - require:
      - test: dep_1

not_executed_1:
  test.nop

not_executed_2:
  test.nop

dep_1:
  test.nop:
  - require:
    - test: dep_2

dep_2:
  test.nop

another_target:
  test.succeed_without_changes:
    - resource_name: the_name

nested_resources:
  test.succeed_without_changes:
    - resource_name: test_nested
  test_regex.none_without_changes_regex:
    - require:
      - test: dep_1

independent_target:
  test.succeed_with_arg_bind:
    - parameters:
        name_to_bind: value_to_bind

target_w_arg_bind_codependent:
  test.succeed_with_arg_bind:
    - parameters:
        ${test:independent_target:testing}
