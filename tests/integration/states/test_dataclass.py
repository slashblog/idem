from pytest_idem.runner import run_sls


def test_dataclass_defaults():
    """
    Test populating default values for arguments that are defined as dataclasses
    """
    ret = run_sls(["dataclass"])
    # Verify defaults are populated
    tag = "test_dataclass_|-test_dataclass_flat_|-test_dataclass_flat_|-present"
    assert ret[tag]["result"] is True
    assert ret[tag]["new_state"].get("arg_flat") == {
        "ArgRequiredBool": False,
        "ArgWithNone": "value",
        "ArgWithBoolTrue": True,
        "ArgWithBoolFalse": False,
        "ArgInt": 10,
        "arg_inner": {
            "InnerRequiredBool": True,
            "InnerBoolTrue": True,
            "InnerBoolFalse": True,
            "InnerInt": 20,
        },
    }

    tag = "test_dataclass_|-test_dataclass_list_|-test_dataclass_list_|-present"
    assert ret[tag]["result"] is True
    assert ret[tag]["new_state"].get("arg_list") == [
        {
            "ArgRequiredStr": "test-str-1",
            "ArgRequiredInt": 3,
            "ArgRequiredBool": False,
            "ArgWithBoolTrue": True,
            "ArgWithBoolFalse": False,
            "ArgDefaultInt": 100,
        },
        {
            "ArgRequiredStr": "test-str-2",
            "ArgRequiredInt": 33,
            "ArgRequiredBool": True,
            "ArgDefaultInt": 133,
            "ArgWithBoolTrue": True,
            "ArgWithBoolFalse": False,
        },
    ]

    tag = "test_dataclass_|-test_dataclass_flat_missing_inner_|-test_dataclass_flat_missing_inner_|-present"
    assert ret[tag]["result"] is True
    assert ret[tag]["new_state"].get("arg_flat") == {
        "ArgRequiredBool": True,
        "ArgWithNone": "newValue",
        "ArgWithBoolTrue": True,
        "ArgWithBoolFalse": False,
        "ArgInt": 10,
    }

    tag = "test_dataclass_|-test_dataclass_only_required_combo_|-test_dataclass_only_required_combo_|-present"
    assert ret[tag]["result"] is True
    assert ret[tag]["new_state"].get("arg_flat") == {
        "ArgRequiredBool": False,
        "ArgWithNone": "value",
        "ArgWithBoolTrue": True,
        "ArgWithBoolFalse": False,
        "ArgInt": 10,
        "arg_inner": {
            "InnerRequiredBool": True,
            "InnerBoolTrue": True,
            "InnerBoolFalse": True,
            "InnerInt": 20,
        },
    }
    assert ret[tag]["new_state"].get("arg_list") == [
        {
            "ArgRequiredStr": "test-str-1",
            "ArgRequiredInt": 3,
            "ArgRequiredBool": False,
            "ArgWithBoolTrue": True,
            "ArgWithBoolFalse": False,
            "ArgDefaultInt": 100,
        },
        {
            "ArgRequiredStr": "test-str-2",
            "ArgRequiredInt": 33,
            "ArgRequiredBool": True,
            "ArgDefaultInt": 133,
            "ArgWithBoolTrue": True,
            "ArgWithBoolFalse": False,
        },
    ]

    tag = "test_dataclass_|-test_dataclass_inner_tree_|-test_dataclass_inner_tree_|-present"
    assert ret[tag]["result"] is True
    assert ret[tag]["new_state"].get("arg_dc_default_in_inner") is not None
    assert ret[tag]["new_state"]["arg_dc_default_in_inner"].get("arg_inner") == {
        "InnerNone": "InnerInt_default_20",
        "InnerBoolTrue": True,
        "InnerBoolFalse": False,
        "InnerInt": 20,
    }
    assert ret[tag]["new_state"]["arg_dc_default_in_inner"].get(
        "arg_inner_different_default"
    ) == {
        "InnerNone": "InnerInt_default_40",
        "InnerBoolTrue": True,
        "InnerBoolFalse": False,
        "InnerInt": 40,
    }

    tag = "test_dataclass_|-test_dataclass_list_within_list_|-test_dataclass_list_within_list_|-present"
    assert ret[tag]["result"] is True
    assert ret[tag]["new_state"]["arg_list"][0].get("arg_inner_list") is not None
    assert len(ret[tag]["new_state"]["arg_list"][0].get("arg_inner_list")) == 2
    assert ret[tag]["new_state"]["arg_list"][0].get("arg_inner_list") == [
        {
            "InnerListRequiredStr": "inner-1",
            "InnerListBoolFalse": False,
            "InnerListDefaultInt": 111,
        },
        {
            "InnerListRequiredStr": "inner-2",
            "InnerListBoolFalse": False,
            "InnerListDefaultInt": 111,
        },
    ]

    tag = "test_dataclass_|-test_dataclass_list_within_list_negative_|-test_dataclass_list_within_list_negative_|-present"
    assert ret[tag]["result"] is False
    assert (
        "states.test_dataclass.present is missing required argument(s): InnerListRequiredStr"
        in ret[tag]["comment"][0]
    ), ret[tag]["comment"][0]
