service1:
  sls.run:
    - sls_sources:
      - sls.invalid_file_path
    - params:
        - params.invalid_params_file
    - db_parameter_group_name: service1_db_parameter_group

first thing:
  test.succeed_with_changes

multi_result_new_state_1:
  test.present:
    - result: True
    - changes: 'skip'
    - new_state:
        "resource_id_1": "resource-1"
        "name": "subnet1"
        "resource_id_2": "resource-2"
        "resource_id_3": "resource-3"
