nested_service:
  sls.run:
    - sls_sources:
      - "../relative_path/include_file1.sls"
    - params:
        - "../params/file1.sls"
    - db_parameter_group_name: service1_db_parameter_group
