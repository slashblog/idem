test_include_sls_run_3:
  test.present:
    - resource_id: idem-test-3
    - new_state:
        key: {{ params[ "key" ] }}
        arg_bind: {{ params[ "arg_bind" ] }}
        resource_id: idem-test-3
        arg_bind_ref: nested_val_1
    - result: true


test_include_sls_run_4:
  test.present:
    - resource_id: idem-test-3
    - new_state:
        key: {{ params[ "key" ] }}
        arg_bind: {{ params[ "arg_bind" ] }}
        resource_id: idem-test-3
        arg_bind_group: idem-${test:test_include_sls_run_3:key}
        arg_bind_ref: nested_val_2
    - result: true
