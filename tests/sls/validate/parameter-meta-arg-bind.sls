META:
  name: meta-name11

{% set resource_name = params.get('name', 'param-name-1') %}

{{resource_name}}:
  META:
    name: meta-name-2
    parameters:
      resource_name:
        description: "The name of the resource."
        name: "Resource name"
        uiElement: text
  test.state.present:
  - name: {{resource_name}}
  - tags:
    - Key: Name
      Value: {{resource_name}}
