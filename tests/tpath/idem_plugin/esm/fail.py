import pathlib
from typing import Any
from typing import Dict


async def get_state(hub, ctx) -> Dict[str, Any]:
    raise ValueError("get_state failure")


async def set_state(hub, ctx, state: Dict[str, Any]):
    raise ValueError("set_state failure")


async def enter(hub, ctx):
    raise ValueError("enter failure")


async def exit_(hub, ctx, handle: pathlib.Path, exception: Exception):
    raise ValueError("exit failure")
