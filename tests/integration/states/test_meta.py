from pytest_idem.runner import run_yaml_block

STATE = """
META:
  top-level-meta: True
  key1: value1

state:
  META:
    state-level-meta: True
    key2: value2
  meta.present:
    - name: test_state
"""


def test_meta_ctx():
    ret = run_yaml_block(STATE)
    state_ret = next(iter(ret.values()))
    assert state_ret["result"], state_ret["comment"]
    assert state_ret["old_state"] == {"top-level-meta": True, "key1": "value1"}
    assert state_ret["new_state"] == {"state-level-meta": True, "key2": "value2"}
