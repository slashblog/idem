FROM python:3.10-slim
SHELL ["/bin/bash", "-c", "-i"]

ADD . /tmp/idem-src
RUN python3 -m pip install /tmp/idem-src

COPY docker/entrypoint.sh /entrypoint.sh
COPY run.py run.py

ENTRYPOINT ["/entrypoint.sh"]
