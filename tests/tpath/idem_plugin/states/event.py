async def flush(hub, ctx, name: str, *args, **kwargs):
    """
    Flush the event queue into the new_state
    """
    ret = {
        "name": name,
        "old_state": None,
        "new_state": [],
        "result": True,
        "comment": [],
    }
    while True:
        if hub.evbus.BUS.empty():
            break
        event = await hub.evbus.BUS.get()
        ret["new_state"].append(event)
    return ret
